local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath("data").."/site/pack/packer/opt/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
  print("Setting up packer.nvim.")
  execute("!git clone https://github.com/wbthomason/packer.nvim "..install_path)
  print("Finished packer.nvim setup.")
end

vim.cmd [[packadd packer.nvim]]

return require("packer").startup(function()
  use {"wbthomason/packer.nvim", opt = true}

  use {"svermeulen/vimpeccable"}

  use {"norcalli/nvim_utils"}

  use {
    "sainnhe/edge",
    config = function()
      local log = require("modules.log")
      log.debug([[Started "edge" config]])
      vim.o.background = "light"
      vim.o.termguicolors = true
      vim.api.nvim_command("colorscheme edge")
      log.debug([[Finished "edge" config]])
    end,
  }

  use {
    "nvim-treesitter/nvim-treesitter",
    config = function()
      local log = require("modules.log")
      log.debug([[Started "nvim-treesitter" config]])
      require("modules.treesitter").setup()
      log.debug([[Finished "nvim-treesitter" config]])
    end,
  }

  use {
    "nvim-lua/completion-nvim",
    requires = {
      "steelsojka/completion-buffers",
      "nvim-treesitter/completion-treesitter",
      "kristijanhusak/completion-tags",
      "albertoCaroM/completion-tmux",
    },
    config = function()
      local log = require("modules.log")
      log.debug([[Started "completion-nvim" config]])
      require("modules.completion").setup()
      log.debug([[Finished "completion-nvim" config]])
    end,
  }

  use {
    "neovim/nvim-lspconfig",
    requires = {"tjdevries/nlua.nvim"},
    config = function()
      local log = require("modules.log")
      log.debug([[Started "nvim-lspconfig" config]])
      require("modules.lsp").setup()
      log.debug([[Finished "nvim-lspconfig" config]])
    end,
  }

  use {
    "glepnir/galaxyline.nvim",
    branch = "main",
    config = function()
      local log = require("modules.log")
      log.debug([[Started "galaxyline.nvim" config]])
      require("modules.statusline")
      log.debug([[Finished "galaxyline.nvim" config]])
    end,
    requires = {"kyazdani42/nvim-web-devicons", opt = true}
  }

  use {
    "tpope/vim-fugitive",
    opt = true,
    requires = {{"tpope/vim-rhubarb", opt = true}},
    config = function()
      local log = require("modules.log")
      log.debug([[Started "vim-fugitive" config]])
      require("modules.git").setup()
      log.debug([[Finished "vim-fugitive" config]])
    end,
    cmd = {"Gstatus", "Gpull", "Gpush", "Gwrite"},
    keys = {
      {"n", "<leader>gs"},
      {"n", "<leader>ga"},
    }
  }

  use {
    "vim-test/vim-test",
    opt = true,
    requires = {{"esamattis/slimux", opt = true}},
    config = function()
      local log = require("modules.log")
      log.debug([[Started "vim-test" config]])
      require("modules.test").setup()
      log.debug([[Finished "vim-test" config]])
    end,
    cmd = {"TestNearest", "TestFile", "TestSuite", "TestLast", "TestVisit"},
  }

  use {
    "mhartington/formatter.nvim",
    opt = true,
    config = function()
      local log = require("modules.log")
      log.debug([[Started "formatter.nvim" config]])
      require("modules.formatter").setup()
      log.debug([[Finished "formatter.nvim" config]])
    end,
    cmd = {"Format"},
  }

  use {"editorconfig/editorconfig-vim"}

  use {"romainl/vim-cool"}

  use {
    "nvim-telescope/telescope.nvim",
    requires = {{"nvim-lua/popup.nvim"}, {"nvim-lua/plenary.nvim"}},
    config = function()
      local log = require("modules.log")
      log.debug([[Started "telescope.nvim" config]])
      require("modules.telescope").setup()
      log.debug([[Finished "telescope.nvim" config]])
    end,
  }

  use {
    "norcalli/snippets.nvim",
    config = function()
      local log = require("modules.log")
      log.debug([[Started "snippets.nvim" config]])
      require("modules.snippets").setup()
      log.debug([[Finished "snippets.nvim" config]])
    end,
  }
end)
