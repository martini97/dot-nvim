local gl = require("galaxyline")
local gls = gl.section
local vcs = require("galaxyline.provider_vcs")
local fileinfo = require('galaxyline.provider_fileinfo')
local diagnostic = require('galaxyline.provider_diagnostic')

local colors = {
  fg = '#4b505b',
  bg = '#eef1f4',
  yellow = '#be7e05',
  green = '#608e32',
  purple = '#b05ccc',
  blue = '#5079be',
  red = '#d05858',
  magenta = '#cc00cc',
}

local buffer_not_empty = function()
  if vim.fn.empty(vim.fn.expand('%:t')) ~= 1 then
    return true
  end
  return false
end

local diagnostic_condition = function()
  local diagnostics = {
    diagnostic.get_diagnostic_error(),
    diagnostic.get_diagnostic_warn(),
  }
  for _, v in pairs(diagnostics) do
    if string.len(v) < 1 then return false end
  end
  return true
end

local function get_mode_hihglight()
  local mode_color = {
    n = colors.blue,
    i = colors.green,
    c = colors.red,
    v = colors.yellow,
    V = colors.yellow,
    [''] = colors.yellow,
    R = colors.purple,
    t = colors.red,
  }
  return {colors.bg, mode_color[vim.fn.mode()]}
end

gls.left[1] = {
  ViMode = {
    provider = function()
      local alias = {
	n = "NORMAL",
	i = "INSERT",
	c = "COMMAND",
	v = "VISUAL",
	V = "VISUAL",
	[''] = "CTRL-V",
	R = "REPLACE",
	t = "TERMINAL",
      }
      vim.api.nvim_command(
	string.format("hi GalaxyViMode guibg=%s guifg=%s gui=bold", unpack(get_mode_hihglight()))
      )
      return "  " .. alias[vim.fn.mode()] .. " "
    end,
    separator = '|',
    separator_highlight = {colors.fg},
  },
}

gls.left[2] = {
  GitIcon = {
    provider = function()
      return '  ' .. vcs.get_git_branch() .. ' '
    end,
    condition = vcs.check_git_workspace,
    highlight = {colors.magenta,colors.bg},
    separator = '|',
    separator_highlight = {colors.fg},
  },
}

gls.left[2] = {
  Git = {
    provider = function()
      local branch = vcs.get_git_branch()
      if not branch then return '' end
      return '  ' .. branch .. ' '
    end,
    condition = vcs.check_git_workspace,
    highlight = {colors.magenta,colors.bg},
    separator = '|',
    separator_highlight = {colors.fg},
  }
}
gls.left[3] ={
  FileIcon = {
    provider = function ()
      return ' ' .. fileinfo.get_file_icon()
    end,
    condition = buffer_not_empty,
    highlight = {fileinfo.get_file_icon_color,colors.bg},
  },
}
gls.left[4] = {
  FileName = {
    provider = {'FileName'},
    condition = buffer_not_empty,
    highlight = {colors.fg,colors.bg,'bold'},
    separator = '|',
    separator_highlight = {colors.fg},
  }
}
gls.left[5] = {
  DiagnosticError = {
    provider = 'DiagnosticError',
    condition = diagnostic_condition,
    icon = '  ',
    highlight = {colors.red,colors.bg}
  }
}
gls.left[6] = {
  Space = {
    provider = function () return ' ' end,
    condition = diagnostic_condition,
    highlight = {colors.bg,colors.bg}
  }
}
gls.left[7] = {
  DiagnosticWarn = {
    provider = 'DiagnosticWarn',
    condition = diagnostic_condition,
    icon = '  ',
    highlight = {colors.blue,colors.bg},
  }
}


gls.right[1] = {
  LineInfo = {
    provider = 'LineColumn',
    highlight = {colors.fg,colors.line_bg},
  },
}
gls.right[2] = {
  LinePercent = {
    provider = "LinePercent",
    separator = '|',
    separator_highlight = {colors.fg},
  }
}
gls.right[3] = {
  ScrollBar = {
    provider = "ScrollBar",
    separator = '|',
    separator_highlight = {colors.fg},
  }
}
