local M = {}

function M.setup()
  vim.g["test#strategy"] = "slimux"
  vim.g["test#javascript#runner"] = "jest"
  vim.g["test#javascript#jest#executable"] = "CI=true yarn test"
end

return M
