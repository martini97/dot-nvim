local M = {}

function M.setup()
  vim.g.completion_enable_snippet = "snippets.nvim"
  vim.g.completion_chain_complete_list = {
    default = {
      {complete_items = {"lsp", "snippet", "buffers", "tags", "tmux"}},
      {mode = "<c-p>"},
      {mode = "<c-n>"},
    },
    python = {
      {complete_items = {"lsp", "snippet", "buffers", "tags", "tmux", "ts"}},
    },
    lua = {
      {complete_items = {"lsp", "snippet", "buffers", "tags", "tmux", "ts"}},
    },
    c = {
      {complete_items = {"lsp", "snippet", "buffers", "tags", "tmux", "ts"}},
    },
  }
  vim.api.nvim_exec([[
    augroup Completion
      autocmd!
      autocmd BufEnter * lua require'completion'.on_attach()
    augroup END
  ]], true)
end

return M
