local M = {}

local function set_indent(width)
  vim.api.nvim_buf_set_option(0, "shiftwidth", width)
  vim.api.nvim_buf_set_option(0, "tabstop", width)
  vim.api.nvim_buf_set_option(0, "softtabstop", width)
end

local function set_lsp()
  vim.api.nvim_buf_set_option(0, "omnifunc", "v:lua.vim.lsp.omnifunc")
end

function M.py()
  vim.g["test#python#runner"] = "pytest"

  set_indent(4)
  set_lsp()
  vim.api.nvim_buf_set_option(0, "errorformat", "%f:%l:%c: %t%n %m")
  vim.api.nvim_buf_set_option(0, "makeprg", "flake8 %:S")
  vim.api.nvim_buf_set_option(0, "formatprg", "black --quiet -")

  local workon_bin = (os.getenv("WORKON_HOME") or "") .. "/neovim/bin"
  vim.env.PATH = ".venv/bin:" .. workon_bin .. ":" .. vim.env.PATH
end

function M.js()
  set_indent(2)
  set_lsp()
  vim.api.nvim_buf_set_option(0, "formatprg", "prettier --parser typescript")
  vim.api.nvim_buf_set_option(0, "makeprg", "eslint -f compact %")
  vim.api.nvim_buf_set_option(0, "formatexpr", "")
  vim.api.nvim_buf_set_option(0, "suffixesadd", ".js,.jsx,.ts,.tsx")
  vim.api.nvim_buf_set_option(0, "path", ".,src")
  vim.env.PATH = "node_modules/.bin:" .. vim.env.PATH
end

return M
