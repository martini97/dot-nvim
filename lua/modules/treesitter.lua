local treesitter = require('nvim-treesitter.configs')
local parsers = require('nvim-treesitter.parsers')

local M = {}

function reload_highlight()
  local bufnr = vim.api.nvim_get_current_buf()
  local lang = parsers.get_buf_lang(bufnr)

  vim.api.nvim_command('write')
  vim.api.nvim_command('edit')
  treesitter.attach_module('highlight', bufnr, lang)
end

function M.setup()
  treesitter.setup({
    highlight = { enable = true },
    indent = { enable = true },
    incremental_selection = { enable = true },
    ensure_installed = { "typescript", "python", "javascript", "tsx", "lua" },
  })

  vimp.map_command("TreesiterReloadHighlight", reload_highlight)
end

return M
