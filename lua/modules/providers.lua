local utils = require('modules.utils')

local function python3_provider()
  local neovim_python = (os.getenv("WORKON_HOME") or "") .. "/neovim/bin/python"
  if vim.fn.executable(neovim_python) then
    return neovim_python
  end
  return "python3"
end

local function ruby_provider()
  local provider = vim.fn.exepath("neovim-ruby-host")
  if utils.file_exists(provider) then return provider end

  provider = vim.fn.expand("~/.gem/ruby/*/bin/neovim-ruby-host")
  if utils.file_exists(provider) then return provider end
end

local function node_provider()
  local provider = vim.fn.exepath("neovim-node-host")
  if utils.file_exists(provider) then return provider end

  provider = vim.fn.expand("~/.config/nvm/versions/node/*/bin/neovim-node-host")
  if utils.file_exists(provider) then return provider end
end

local M = {}

function M.setup()
  vim.g.loaded_python_provider = 0
  vim.g.loaded_perl_provider = 0
  vim.g.python3_host_prog = python3_provider()
  vim.g.node_host_prog = node_provider()
  vim.g.ruby_host_prog = ruby_provider()
end

return M
