local snippets = require("snippets")
local s_utils = require("snippets.utils")
local M = {}

snippets.snippets = {
  lua = {
    req = [[local ${2:${1|S.v:match"([^.()]+)[()]*$"}} = require '$1']];
    func = [[function${1|vim.trim(S.v):gsub("^%S"," %0")}(${2|vim.trim(S.v)})$0 end]];
    ["local"] = [[local ${2:${1|S.v:match"([^.()]+)[()]*$"}} = ${1}]];
    ["for"] = s_utils.match_indentation [[
for ${1:i}, ${2:v} in ipairs(${3:t}) do
  $0
end]];
  };
  javascript = {
    switch = s_utils.match_indentation [[
switch (${1}) {
  case ${2}:
    $0
    break;
  default:
    return
} ]];
    useS = [[const [${1}, set${1|S.v:gsub("(%l)(%w*)", function(a,b) return string.upper(a)..b end)}] = useState($2)$0]]
  };
  typescriptreact = {
    newComponent = [[
import React from 'react'

interface Props {
}

export default function ${1} ({ }: Props) {
  return (
    <div />
  )
} ]];
    switch = s_utils.match_indentation [[
switch (${1}) {
  case ${2}:
    $0
    break;
  default:
    return
} ]];
    useS = [[const [${1}, set${1|S.v:gsub("(%l)(%w*)", function(a,b) return string.upper(a)..b end)}] = useState($2)$0]]
  };
  _global = {
    date = function() return os.date("%Y-%m-%d") end;
    uuid = function() return io.popen("uuidgen"):read"*l" end;
    copyright = s_utils.force_comment [[Copyright (C) Alessandro Martini ${=os.date("%Y")}]];
  };
}

function M.setup()
  snippets.use_suggested_mappings()
end

return M
