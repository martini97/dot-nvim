local M = {}

function M.file_exists(path)
  local ok, _, code = os.rename(path, path)
  if not ok then
    if code == 13 then
      -- Permission denied, but it exists
      return true
    end
  end

  return ok
end

return M
