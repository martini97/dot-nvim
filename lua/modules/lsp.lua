local log = require("modules.log")
local lsp = require("lspconfig")
local completion = require("completion")
local M = {}

local eslint = {
  lintCommand = "eslint -f unix --stdin --stdin-filename ${INPUT}",
  lintIgnoreExitCode = true,
  lintStdin = true,
}

local servers = {
  tsserver = {},
  vimls = {
    cmd = { vim.fn.stdpath('cache') .. "/lspconfig/vimls/node_modules/.bin/vim-language-server", "--stdio" }
  },
  pyright = {},
  efm = {
    before = function (client)
      client.resolved_capabilities.goto_definition = false
      client.resolved_capabilities.find_references = false
      client.resolved_capabilities.declaration = false
      client.resolved_capabilities.rename = false
    end,
    init_options = { documentFormatting = true },
    settings = {
      rootMarkers = {".git/"},
      languages = {
        typescriptreact = {eslint},
        typescript = {eslint},
        javascript = {eslint},
        javascriptreact = {eslint},
      },
    }
  },
}

local function make_on_attach(config)
  return function (client)
    log.debug(string.format([[Attaching client "%s"]], client.name))

    if config.before then
      config.before(client)
    end

    completion.on_attach(client)

    local mappings = {
      {"[e", vim.lsp.diagnostic.goto_next},
      {"]e", vim.lsp.diagnostic.goto_prev},
    }

    if client.resolved_capabilities.goto_definition then
      table.insert(mappings, {"gd", vim.lsp.buf.definition})
    end

    if client.resolved_capabilities.find_references then
      table.insert(mappings, {"gr", vim.lsp.buf.references})
    end

    if client.resolved_capabilities.declaration then
      table.insert(mappings, {"gD", vim.lsp.buf.declaration})
    end

    if client.resolved_capabilities.rename then
      table.insert(mappings, {"gR", vim.lsp.buf.rename})
    end

    for _, map in pairs(mappings) do
      -- NOTE: need the override here when reloading the LSP
      vimp.add_buffer_maps(function()
        vimp.nnoremap({"override"}, map[1], function() map[2]() end)
      end)
    end

    if config.after then
      config.after(client)
    end

    vim.api.nvim_command(
      [[autocmd CursorHoldI <buffer> lua vim.lsp.buf.document_highlight()]])

    vim.api.nvim_command(
      [[autocmd CursorMoved <buffer> lua vim.lsp.util.buf_clear_references()]])

    log.debug(string.format([[Finished attaching client "%s"]], client.name))
  end
end

local function sumneko_command()
  local cache_location = vim.fn.stdpath('cache')
  local bin_location = jit.os
  if vim.fn.has('mac') then
    bin_location = 'macOS'
  end

  return {
    string.format(
      "%s/lspconfig/sumneko_lua/lua-language-server/bin/%s/lua-language-server",
      cache_location,
      bin_location
    ),
    "-E",
    string.format(
      "%s/nvim/lspconfig/sumneko_lua/lua-language-server/main.lua",
      cache_location
    ),
}
end

function M.setup()
  require("nlua.lsp.nvim").setup(lsp, {
    on_attach = make_on_attach({}),
    cmd = sumneko_command(),
    globals = {
      -- packer.nvim
      "use"
    }
  })

  for server, config in pairs(servers) do
    config = config or {}
    config.on_attach = make_on_attach(config)
    lsp[server].setup(config)
  end
end

return M
