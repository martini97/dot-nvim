local M = {}

local function keybindings(config)
  local keymaps = {
    s = ":Gstatus<cr>",
    a = ":Gwrite",
  }

  for key, value in pairs(keymaps) do
    local keymap = string.format("<leader>%s%s", config.base_key, key)
    vimp.nnoremap(keymap, value)
  end
end

function M.setup(config)
  config = config or {}
  config.base_key = config.base_key or "g"

  keybindings(config)
end

return M
