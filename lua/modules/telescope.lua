local t_builtin = require('telescope.builtin')
local M = {}

local mappings = {
  i = {
    ["<c-k>"] = require('telescope.mappings').default_mappings.i["<C-p>"],
    ["<c-j>"] = require('telescope.mappings').default_mappings.i["<C-n>"],
    ["<esc>"] = require('telescope.actions').close,
  },
}

local function keybindings(config)
  local keymaps = {
    f = {"find_files"},
    g = {"live_grep"},
    b = {"buffers", {show_all_buffers = true}},
    h = {"help_tags"},
    t = {"tags"},
    q = {"quickfix"},
  }

  for key, value in pairs(keymaps) do
    local filter = value[1]
    local opts = value[2] or {}
    local keymap = string.format("<leader>%s%s", config.base_key, key)
    vimp.nnoremap(keymap, function() t_builtin[filter](opts) end)
  end
end

function M.setup(config)
  config = config or {}
  config.base_key = config.base_key or "f"

  keybindings(config)

  require('telescope').setup({
    defaults = {
      mappings = mappings,
      color_devicons = false,
    }
  })
end

return M
