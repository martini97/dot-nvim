require("vimp")
require("nvim_utils")

require("plugins")
require("modules.providers").setup()

local filetypes = require("modules.filetypes")

vim.g.mapleader = " "
vim.g.maplocalleader = ","

vim.o.shortmess = vim.o.shortmess .. "c"
vim.o.completeopt = "menuone,noinsert,noselect"
vim.o.shiftwidth = 2
vim.o.softtabstop = 2
vim.o.tabstop = 2
vim.o.expandtab = true
vim.o.autoindent = false
vim.bo.shiftwidth = 2
vim.bo.softtabstop = 2
vim.o.tabstop = 2
vim.bo.expandtab = true
vim.bo.autoindent = false
vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.colorcolumn = "80,100"
vim.o.updatetime = 300
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.inccommand = "nosplit"
vim.o.hidden = true

if vim.fn.executable("rg") == 1 then
  vim.o.grepprg = "rg --vimgrep --no-column --smart-case --color=never"
end

local keymaps = {
  c = {
    ["<c-k>"] = "<up>",
    ["<c-j>"] = "<down>",
    ["<c-a>"] = "<home>",
    ["<c-e>"] = "<end>",
  },
  n = {
    ["<space>er"] = ":r <c-r>=expand('%:.:h') . '/'<cr>",
    ["<space>ew"] = ":e <c-r>=expand('%:.:h') . '/'<cr>",
    ["<space>es"] = ":sp <c-r>=expand('%:.:h') . '/'<cr>",
    ["<space>ev"] = ":vs <c-r>=expand('%:.:h') . '/'<cr>",
  },
}

for mode, mappings in pairs(keymaps) do
  for lhs, rhs in pairs(mappings) do
    vimp.bind(mode, lhs, rhs)
  end
end

LUA_AUTOCMD_HOOKS = {
  js = filetypes.js,
  py = filetypes.py,
  yank = function() vim.highlight.on_yank({higroup = "IncSearch", timeout = 200}) end,
}

local autocmds = {
  lua_ft_autocmds = {
    {"Filetype", "javascript,javascriptreact,typescript,typescriptreact", "lua LUA_AUTOCMD_HOOKS.js()"},
    {"Filetype", "python", "lua LUA_AUTOCMD_HOOKS.py()"},
  },
  lua_highlight_yank = {
    {"TextYankPost", "*", "lua LUA_AUTOCMD_HOOKS.yank()"},
  },
}

nvim_create_augroups(autocmds)
