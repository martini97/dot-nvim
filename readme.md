# dot-nvim

Lua based vim config.

![meme](https://user-images.githubusercontent.com/4466899/100768498-f31a6080-33c8-11eb-8cba-bdb73b9fcc4d.png)

## debugging

To launch in debug mode use:

```bash
DOTNVIM_DEBUG_LEVEL=trace nvim
```
